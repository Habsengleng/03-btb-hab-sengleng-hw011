import React, { Component } from "react";
import { Card, Form, Button } from "react-bootstrap";
import calculator from "../img/calculator.png";

var i = 1;
class Calculator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstNum: "",
      secondNum: "",
      operator: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleOpertor = this.handleOpertor.bind(this);
    this.handleOptionChange = this.handleOptionChange.bind(this);
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }
  handleSubmit() {
    if (this.state.firstNum !== "" && !isNaN(this.state.firstNum)) {
      if (this.state.secondNum !== "" && !isNaN(this.state.secondNum)) {
        if (this.state.operator !== "") {
          document.getElementById("error2").innerHTML = "";
          document.getElementById("error1").innerHTML = "";
          document.getElementById("error3").innerHTML = "";
          this.handleOpertor();
        } else {
          document.getElementById("error1").innerHTML = "";
          document.getElementById("error2").innerHTML = "";
          document.getElementById("error3").innerHTML = "Please select option";
        }
      } else {
        document.getElementById("error1").innerHTML = "";
        document.getElementById("error2").innerHTML =
          "Please input valid number";
        document.getElementById("error3").innerHTML = "";
        this.nameInput2.focus();
      }
    } else {
      document.getElementById("error2").innerHTML = "";
      document.getElementById("error1").innerHTML = "Please input valid number";
      document.getElementById("error3").innerHTML = "";
      this.nameInput1.focus();
    }
  }
  handleOptionChange(e) {
    this.setState({
      operator: e.target.value,
    });
  }
  handleOpertor() {
    var result = 0;
    var a = parseFloat(this.state.firstNum);
    var b = parseFloat(this.state.secondNum);
    let operation = this.state.operator;
    if (operation === "+") {
      result = a + b;
    } else if (operation === "*") {
      result = a * b;
    } else if (a > b) {
      if (operation === "-") {
        result = a - b;
      } else if (operation === "/") {
        result = a / b;
      } else if (operation === "%") {
        result = a % b;
      }
    } else {
      result = 0;
    }
    var resultNum = {
      id: i++,
      myResult: result,
    };

    this.props.getResult(resultNum);
  }
  render() {
    return (
      <div>
        <Card style={{ padding: "30px" }}>
          <Card.Img variant="top" src={calculator} /> <br />
          <Form>
            <Form.Group>
              <Form.Control
                type="text"
                name="firstNum"
                value={this.state.firstNum}
                onChange={this.handleChange}
                ref={(input1) => {
                  this.nameInput1 = input1;
                }}
              ></Form.Control>
              <span id="error1" style={{ color: "red" }}></span>
            </Form.Group>
            <Form.Group>
              <Form.Control
                type="text"
                name="secondNum"
                value={this.state.secondNum}
                onChange={this.handleChange}
                ref={(input2) => {
                  this.nameInput2 = input2;
                }}
              ></Form.Control>
              <span id="error2" style={{ color: "red" }}></span>
            </Form.Group>
            <Form.Group>
              <Form.Control
                as="select"
                onChange={this.handleChange}
                name="operator"
                value={this.state.operator}
              >
                <option value="" disabled>
                  Choose Operator
                </option>
                <option value="+">+ Plus</option>
                <option value="-">- Subtract</option>
                <option value="*">* Multiple</option>
                <option value="/">/ Divide</option>
                <option value="%">% Module</option>
              </Form.Control>
              <span id="error3" style={{ color: "red" }}></span>
            </Form.Group>
            <Button variant="primary" onClick={this.handleSubmit}>
              Calculate
            </Button>{" "}
          </Form>
        </Card>
      </div>
    );
  }
}

export default Calculator;
