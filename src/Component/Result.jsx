import React from "react";
import { ListGroup} from "react-bootstrap";
export default function Result(props) {
  var result = props.output;
  const listitem = result.map((result) => (
    <ListGroup.Item key={result.id}>{result.myResult}</ListGroup.Item>
  ));
  return (
    <div>
      <h1>Result History</h1>
      <ListGroup>
        {listitem}
      </ListGroup>
    </div>
  );
}
