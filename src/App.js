import React from "react";
import Calculator from "./Component/Calculator";
import Result from "./Component/Result";
import { Container, Row, Col } from "react-bootstrap";

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      arr: [],
    };
  }
  getData(num) {
    this.setState({
      arr: [...this.state.arr, num],
    });
  }
  render() {
    return (
        <div>
          <Container style={{ marginTop: "20px" }}>
            <Row>
              <Col lg={5} md={6} sm={12}>
                <Calculator getResult={this.getData.bind(this)} />
              </Col>
              <Col lg={7} md={6} sm={12}>
                <Result output={this.state.arr} />
              </Col>
            </Row>
          </Container>
        </div>
    );
  }
}
